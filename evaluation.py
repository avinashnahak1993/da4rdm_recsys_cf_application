import pandas as pd
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, mean_squared_error, roc_curve, \
    ndcg_score, classification_report, auc, roc_auc_score, mean_absolute_error, silhouette_samples, silhouette_score
from scipy.stats import spearmanr
import numpy as np
from roc import compute_ROC
from plotting import plot_ndcg, plot_box_plot, plot_heatmap, plot_silhouette_score, plot_scatter_matrix, \
    plot_precision_recall


def evaluate(df):
    # DO NOT DELETE THE BELLOW LINE OF CODE
    eval_matrix, ground_truth_matrix, similarity_matrix = create_similarity_matrix(df)

    confusion = confusion_matrix(ground_truth_matrix.flatten(), similarity_matrix.flatten())

    plot_heatmap(eval_matrix)
    # plot_box_plot(eval_matrix)

    # Compute ROC
    compute_ROC(ground_truth_matrix, similarity_matrix, confusion, 0)
    # Compute precision, recall, and F1 score
    precision, recall, f1_score, _ = precision_recall_fscore_support(ground_truth_matrix.flatten(),
                                                                     similarity_matrix.flatten(), average='macro')
    # Compute mean squared error (MSE)
    mse = mean_squared_error(ground_truth_matrix.flatten(), similarity_matrix.flatten())
    rmse = mean_squared_error(ground_truth_matrix.flatten(), similarity_matrix.flatten(), squared=False)
    # compute mean absolute error (MAE)
    mae = mean_absolute_error(ground_truth_matrix.flatten(), similarity_matrix.flatten())
    # Compute NDCG score
    ndcg = ndcg_score(ground_truth_matrix, similarity_matrix)
    # plot_ndcg(ndcg)
    # Compute Spearman's Rank Correlation Coefficient
    spearman_corr, _ = spearmanr(ground_truth_matrix.flatten(), similarity_matrix.flatten())
    plot_scatter_matrix(ground_truth_matrix, similarity_matrix)
    plot_precision_recall(ground_truth_matrix, similarity_matrix)

    # print("Confusion matrix:\n", confusion)
    print("classification_report: \n",
          classification_report(ground_truth_matrix.flatten(), similarity_matrix.flatten()))
    print("Precision: {:.4f}, Recall: {:.4f}, F1 score: {:.4f}".format(precision, recall, f1_score))
    print("Mean Squared Error (MSE): {:.4f}".format(mse))
    print("Root Mean Squared Error (RMSE): {:.4f}".format(rmse))
    print("Mean Average Error (MAE): {:.4f}".format(mae))
    print("NDCG score: {:.4f}".format(ndcg))
    print("Spearman's Rank Correlation Coefficient: {:.4f}".format(spearman_corr))
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>> Silhouette Evaluation <<<<<<<<<<<<<<<<<<<<<<<<")
    # calculate silhouette scores for each sample in the confusion matrix
    silhouette_vals = silhouette_samples(ground_truth_matrix, np.argmax(ground_truth_matrix, axis=1))
    # calculate mean silhouette score for the entire dataset
    silhouette_avg = silhouette_score(similarity_matrix, np.argmax(similarity_matrix, axis=1))
    plot_silhouette_score(silhouette_vals, silhouette_avg, ground_truth_matrix)
    print("silhouette_vals score:\n", silhouette_vals)
    print("silhouette_avg score: ", silhouette_avg)


def create_similarity_matrix(eval_matrix):
    eval_matrix.set_index('ResourceId', inplace=True)
    eval_matrix.to_csv(
        "Data/Eval_matrix.csv")
    # Converting resource names into an array of int
    ground_truth_matrix = eval_matrix.to_numpy(dtype=int)
    similarity_matrix = eval_matrix.to_numpy(dtype=int)
    pd.DataFrame(similarity_matrix).to_csv(
        "Data/Similarity_matrix.csv")
    return eval_matrix, ground_truth_matrix, similarity_matrix
